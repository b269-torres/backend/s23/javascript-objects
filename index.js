// Real World Application Of Objects
/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/
let playerInfo = {
    playerName: 'Ash Ketchum',
    playerAgeage: 10,
    friends: {haenn: ["May", "Max"], 
    kanto: ["Brock", "Misty"]},
    pokemon: ["Pickachu", "Charizard", "Squirtle", "Bulbassaur"],
    talk: function(){
        console.log( playerInfo.pokemon[0] + '! I Choose you');
    }

}
console.log(playerInfo);
console.log('Result of dot notation:');
console.log(playerInfo.playerName);
console.log('Result of square Bracket notation:')
console.log(playerInfo['pokemon']);
console.log('Result of the talk method')
playerInfo.talk();




    function Pokemon(name, level){
        this.name = name;
        this.level = level;
        this.health = Math.round(((this.level*this.level)/2));
        this.attack = this.level*2;

        this.tackle = function(target){
            let healthAfterDamage = target.health - this.attack;
                target.health = healthAfterDamage;
            console.log(this.name + " tackeled "+ target.name);
            if(target.health <=0){
                this.faint();
            }else{
            console.log(target.name + "'s health is now reduced to "+ healthAfterDamage );
            }
        }
        this.faint= function(){
            console.log(this.name + " fainted.")
        }
    }

    let rattata = new Pokemon("Rattata", 30);
    let pikachu = new Pokemon("Pikachu", 25);
    let mewtwo = new Pokemon("Mewtwo",40);

    console.log(rattata);
    console.log(pikachu);
    console.log(mewtwo);

    
    mewtwo.tackle(pikachu);

    mewtwo.tackle(pikachu);
    mewtwo.tackle(pikachu);

    rattata.tackle(pikachu);
        console.log(pikachu);
    rattata.tackle(pikachu);
